# Info

Hello, I decided to make this repository to hold links to various blogs and journals that people want others to read.  

If you want me to list your journal, you may contact me on discord via a message.  No LGBTQ stuff, I want to focus strictly on technology and source code with this, thank you.  

## Filters for links to be added.  

* No politics, software projects are NOT to be politicized, period.  
* Be friendly, I much prefer things remain civil.  
* If your blog is full of bull, forget having it listed, I want it fact checked before you request for it to be linked.  
* Be adults, if you are going to engage in name-calling or acting like a child, your posts will not be linked.  
* If you have read-write access to this repository, don't add links to your own blogs without the permission of this repository's owner.  

<table>
    <tr><td><b>ID</b></td><td><b>Author</b></td><td><b>Blog</b></td><td><b>Discription</b></td></tr>
    <tr><td>8080</td><td>Joe123</td><td>Foo</td><td>bar</td><td><i>example</i></td></tr>
</table>